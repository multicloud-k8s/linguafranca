package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"

	"github.com/gobuffalo/packr/v2"
	"gopkg.in/yaml.v2"
)

type group struct {
	Name    string
	Type    string
	Min     int
	Desired int
	Max     int
}

type cluster struct {
	Name       string
	Region     string
	NodeGroups []group `yaml:"nodeGroups"`
}

type provider struct {
	Kind     string
	Clusters []cluster
}

type config struct {
	Bootstrap string
	Providers []provider
}

func getConfig(path string) (*config, error) {
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	c := config{}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}

const backend = `
terraform {
	required_version = ">= 0.12.0"

	backend "s3" {
		bucket = "k8s-multicloud-state"
		key    = "linguafranca"
		region = "eu-west-1"
	}
}
`

func loadFluxYaml(bootstrap string) (string, error) {
	box := packr.New("flux", "./assets/flux")

	dir, err := ioutil.TempDir("", "")
	if err != nil {
		return "", err
	}

	wf := func(path string, file packr.File) error {
		content := []byte(fmt.Sprint(file))
		tmpfn := filepath.Join(dir, path)
		if err := ioutil.WriteFile(tmpfn, content, 0666); err != nil {
			return err
		}

		return nil
	}

	err = box.Walk(wf)
	if err != nil {
		return "", err
	}

	return dir, nil
}

func main() {
	path := flag.String("config-file", "", "path to config file")
	flag.Parse()

	if path == nil || len(*path) == 0 {
		log.Fatal("Config file path cant be empty")
	}

	c, err := getConfig(*path)
	if err != nil {
		log.Fatalf("Could not parse config: %v", err)
	}

	if len(c.Providers) == 0 {
		log.Fatalf("Provider list cant be empty")
	}

	fluxDir, err := loadFluxYaml(c.Bootstrap)
	if err != nil {
		log.Fatalf("Could not load flux yaml: %v", err)
	}

	output := backend + "\n"
	for _, provider := range c.Providers {
		if provider.Kind == "aws" {
			s, err := generateAws(provider, fluxDir)
			if err != nil {
				log.Fatalf("Could not generate terraform: %v", err)
			}

			output = output + s
		} else if provider.Kind == "gcp" {
			s, err := generateGcp(provider, fluxDir)
			if err != nil {
				log.Fatalf("Could not generate terraform: %v", err)
			}

			output = output + s
		} else if provider.Kind == "azure" {
			s, err := generateAzure(provider, fluxDir)
			if err != nil {
				log.Fatalf("Could not generate terraform: %v", err)
			}

			output = output + s
		} else {
			log.Fatalf("Cloud provider is not supported: %v", provider.Kind)
		}
	}

	fmt.Println(output)
}
