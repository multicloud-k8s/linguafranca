FROM golang:1.13-alpine AS build
RUN apk add --no-cache git
RUN go get github.com/golang/dep/cmd/dep
COPY Gopkg.lock Gopkg.toml /go/src/gitlab.com/multicloud-k8s/linguafranca/
WORKDIR /go/src/gitlab.com/multicloud-k8s/linguafranca/
RUN dep ensure -vendor-only
RUN go get -u github.com/gobuffalo/packr/v2/packr2
COPY . /go/src/gitlab.com/multicloud-k8s/linguafranca/
RUN CGO_ENABLED=0 packr2 build -o /bin/linguafranca .

FROM alpine
COPY --from=build /bin/linguafranca /bin/linguafranca
RUN apk add curl

RUN wget https://releases.hashicorp.com/terraform/0.12.16/terraform_0.12.16_linux_amd64.zip
RUN unzip terraform_0.12.16_linux_amd64.zip
RUN mv terraform /bin/terraform
RUN rm terraform_0.12.16_linux_amd64.zip

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
RUN chmod +x kubectl
RUN mv kubectl /bin/kubectl

RUN curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
RUN chmod +x aws-iam-authenticator
RUN mv aws-iam-authenticator /bin/aws-iam-authenticator

RUN apk add python3
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

RUN apk add py-pip && \
    apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python-dev make
RUN pip --no-cache-dir install azure-cli

RUN apk add --no-cache bash