package main

import (
	"bytes"
	"text/template"
)

const awsBase = `
provider "aws" {
	version = ">= 2.28.1"
	region  = "eu-west-1"
}

data "aws_availability_zones" "available" {
}
`

const eksTemplate = `
module "vpc-{{ .Cluster.Name }}" {
	source  = "terraform-aws-modules/vpc/aws"
	version = "2.6.0"

	name                 = "{{ .Cluster.Name }}-vpc"
	cidr                 = "10.0.0.0/16"
	azs                  = data.aws_availability_zones.available.names
	private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
	public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
	enable_nat_gateway   = true
	single_nat_gateway   = true
	enable_dns_hostnames = true

	tags = {
		"kubernetes.io/cluster/{{ .Cluster.Name }}" = "owned"
	}

	public_subnet_tags = {
		"kubernetes.io/cluster/{{ .Cluster.Name }}" = "owned"
		"kubernetes.io/role/elb"                      = "1"
	}

	private_subnet_tags = {
		"kubernetes.io/cluster/{{ .Cluster.Name }}" = "owned"
		"kubernetes.io/role/internal-elb"             = "1"
	}
}

module "eks-{{ .Cluster.Name }}" {
	source       = "terraform-aws-modules/eks/aws"
	cluster_name = "{{ .Cluster.Name }}"

	vpc_id = module.vpc-{{ .Cluster.Name }}.vpc_id
	subnets      = module.vpc-{{ .Cluster.Name }}.private_subnets

	manage_aws_auth       = true
	write_aws_auth_config = false
	write_kubeconfig      = true

	worker_groups = [
		{{ range .Cluster.NodeGroups }}
		{
			name                 = "{{ .Name }}"
			instance_type        = "{{ .Type }}"
			asg_desired_capacity = {{ .Desired }}
			asg_min_size              = {{ .Min }}
			asg_max_size              = {{ .Max }}
		},
		{{ end }}
	]

	map_users = [
		{
			userarn = "arn:aws:iam::572113081883:user/PhilipL"
			username = "PhilipL"
			groups = ["system:masters"]
		}
	]
}

resource "null_resource" "eks-bootstrap-{{ .Cluster.Name }}" {
	depends_on = [module.eks-{{ .Cluster.Name }}]

	provisioner "local-exec" {
		command = "kubectl apply --kubeconfig=${module.eks-{{ .Cluster.Name }}.kubeconfig_filename} -k {{ .Flux }}"
	}
}
`

func createEks(c cluster, fluxDir string) (string, error) {
	t := template.Must(template.New("eks").Parse(eksTemplate))

	data := struct {
		Cluster cluster
		Flux    string
	}{
		Cluster: c,
		Flux:    fluxDir,
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		return "", err
	}

	return tpl.String(), nil
}

func generateAws(p provider, fluxDir string) (string, error) {
	output := awsBase + "\n"

	for _, c := range p.Clusters {
		eks, err := createEks(c, fluxDir)
		if err != nil {
			return "", err
		}

		output = output + "\n" + eks
	}

	return output, nil
}
