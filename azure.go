package main

import (
	"bytes"
	"text/template"
)

const azureBase = `
provider "azurerm" {
	version = "=1.36.0"
	subscription_id = var.arm_subscription_id
	client_id       = var.arm_client_id
	client_secret   = var.arm_client_secret
	tenant_id       = var.arm_tenant_id
}

variable "arm_tenant_id" {}

variable "arm_client_secret" {}

variable "arm_client_id" {}

variable "arm_subscription_id" {}

`

const aksTemplate = `
resource "random_id" "cluster_{{ .Name }}" {
	byte_length = 8
}

resource "random_string" "cluster_{{ .Name }}" {
	length = 8
  	number  = false
	special = false
}
  

module "aks-{{ .Name }}" {
	source  = "Azure/aks/azurerm"
	CLIENT_SECRET = var.arm_client_secret
    CLIENT_ID = var.arm_client_id
	prefix = join("-", [random_string.cluster_{{ .Name }}.result, "{{ .Name }}"])
	kubernetes_version = "1.15.5"
	location = "northeurope"
  }

resource "null_resource" "aks-bootstrap-{{ .Name }}" {
	depends_on = [module.aks-{{ .Name }}]
	
	provisioner "local-exec" {
		command = <<EOT
			az login --service-principal --username ${var.arm_client_id} --password "${var.arm_client_secret}" --tenant ${var.arm_tenant_id}
			KUBECONFIG_OLD=$KUBECONFIG
			export KUBECONFIG=./kubeconfig-aks
			az aks get-credentials --name ${random_string.cluster_{{ .Name }}.result}-{{ .Name }}-aks --resource-group ${random_string.cluster_{{ .Name }}.result}-{{ .Name }}-resources -f ./kubeconfig-aks
			kubectl apply -k {{ .Flux }}
			KUBECONFIG=$KUBECONFIG_OLD
		EOT
	}
}
`

func createAks(name string, fluxDir string) (string, error) {
	t := template.Must(template.New("aks").Parse(aksTemplate))

	data := struct {
		Name string
		Flux string
	}{
		Name: name,
		Flux: fluxDir,
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		return "", err
	}

	return tpl.String(), nil
}

func generateAzure(p provider, fluxDir string) (string, error) {
	output := azureBase + "\n"

	for _, c := range p.Clusters {
		aks, err := createAks(c.Name, fluxDir)
		if err != nil {
			return "", err
		}

		output = output + "\n" + aks
	}

	return output, nil
}
