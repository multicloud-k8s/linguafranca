.PHONY: all

VERSION := $(shell git describe --tags --always --dirty)

IMAGE_REGISTRY := registry.gitlab.com/multicloud-k8s
IMAGE_NAME := linguafranca
IMAGE_TAG_NAME := $(VERSION)
IMAGE_REPO := $(IMAGE_REGISTRY)/$(IMAGE_NAME)
IMAGE_TAG := $(IMAGE_REPO):$(IMAGE_TAG_NAME)

all: image

run:
	go run .

image:
	docker build -t $(IMAGE_TAG) .

push: image
	docker push $(IMAGE_TAG)
