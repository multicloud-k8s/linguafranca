package main

import (
	"bytes"
	"text/template"
)

const gcpBase = `
provider "google" {
	version = "~> 2.18.0"
	region = "europe-north1"
	zone = "europe-north1-a"
	project = "multicloud-k8s-259914"
}

locals {
	network_name = "gke-network"
	subnetwork_name = "gke-subnet"
	ip_range_pods_name = "ip-range-pods"
	ip_range_services_name = "ip-range-scv"
}

data "google_client_config" "current" {
}

module "gcp-network" {
	source       = "terraform-google-modules/network/google"
	version      = "~> 1.4.0"
	project_id   = data.google_client_config.current.project
	network_name = local.network_name

	subnets = [
		{
			subnet_name   = local.subnetwork_name
			subnet_ip     = "10.0.0.0/17"
			subnet_region = data.google_client_config.current.region
		},
	]

	secondary_ranges = {
		"${local.subnetwork_name}" = [
			{
				range_name    = local.ip_range_pods_name
				ip_cidr_range = "192.168.0.0/18"
			},
			{
				range_name    = local.ip_range_services_name
				ip_cidr_range = "192.168.64.0/18"
			},
		]
	}
}
`

const gkeTemplate = `
module "gke-{{ .Name }}" {
	source                 = "terraform-google-modules/kubernetes-engine/google"
	project_id             = data.google_client_config.current.project
	name                   = "{{ .Name }}"
	regional               = false
	region                 = data.google_client_config.current.region
	zones                  = [data.google_client_config.current.zone]
	network                = module.gcp-network.network_name
	subnetwork             = module.gcp-network.subnets_names[0]
	ip_range_pods          = local.ip_range_pods_name
	ip_range_services      = local.ip_range_services_name
	create_service_account = true
}

resource "null_resource" "gke-bootstrap-{{ .Name }}" {
	depends_on = [module.gke-{{ .Name }}]

	provisioner "local-exec" {
		command = <<EOT
			KUBECONFIG_OLD=$KUBECONFIG
			export KUBECONFIG=./kubeconfig-gke
			gcloud container clusters get-credentials {{ .Name }} 
			kubectl apply -k {{ .Flux }}
			KUBECONFIG=$KUBECONFIG_OLD
		EOT
	}
}
`

func createGke(name string, fluxDir string) (string, error) {
	t := template.Must(template.New("gke").Parse(gkeTemplate))

	data := struct {
		Name string
		Flux string
	}{
		Name: name,
		Flux: fluxDir,
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		return "", err
	}

	return tpl.String(), nil
}

func generateGcp(p provider, fluxDir string) (string, error) {
	output := gcpBase + "\n"

	for _, c := range p.Clusters {
		gke, err := createGke(c.Name, fluxDir)
		if err != nil {
			return "", err
		}

		output = output + "\n" + gke
	}

	return output, nil
}
